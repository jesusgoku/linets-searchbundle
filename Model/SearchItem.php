<?php

namespace Linets\SearchBundle\Model;

use Linets\SearchBundle\Interfaces\SearchItemInterface;


/**
 * Class SearchItem
 * @package Linets\SearchBundle\Model
 *
 * @author Jesús Urrutia <jesus.urrutia@gmail.com>
 */
class SearchItem implements SearchItemInterface
{
    /** @var string */
    protected $title;

    /** @var string */
    protected $more;

    /** @var string */
    protected $uri;

    /** @var string */
    protected $image;

    /** @var \DateTime */
    protected $published;

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getMore()
    {
        return $this->more;
    }

    /**
     * @param string $more
     *
     * @return $this
     */
    public function setMore($more)
    {
        $this->more = $more;

        return $this;
    }

    /**
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * @param string $uri
     *
     * @return $this
     */
    public function setUri($uri)
    {
        $this->uri = $uri;

        return $this;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     *
     * @return $this
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * @param \DateTime $published
     * @return Search
     */
    public function setPublished($published)
    {
        $this->published = $published;

        return $this;
    }
}
