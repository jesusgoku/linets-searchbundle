<?php

namespace Linets\SearchBundle\Model;

use Linets\SearchBundle\Interfaces\SearchInterface;
use Linets\SearchBundle\Interfaces\SearchItemInterface;


/**
 * Class Search
 * @package Linets\SearchBundle\Model
 *
 * @author Jesús Urrutia <jesus.urrutia@gmail.com>
 */
class Search implements SearchInterface
{
    /** @var  SearchItemInterface[] */
    protected $results;

    /** @var int */
    protected $total;

    /** @var int */
    protected $page;

    /** @var int */
    protected $limit;

    /** @var string */
    protected $title;

    public function __construct(array $results, $title = null, $total = null, $page = 1, $limit = 10)
    {
        $this->results = $results;
        $this->total = null === $total ? count($results) : $total;
        $this->page = $page;
        $this->limit = $limit;
        $this->title = null === $title ? 'Section' : $title;
    }

    /**
     * Return array of search item elements
     *
     * @return SearchItemInterface[]
     */
    public function getResults()
    {
        return $this->results;
    }

    /**
     * Return number of total search item
     *
     * @return integer
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Return page of results
     *
     * @return integer
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * Return limit results per page
     *
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * Return title search
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }
}
