<?php

namespace Linets\SearchBundle\Interfaces;


/**
 * Interface SearchInterface
 * @package Linets\SearchBundle\Interfaces
 *
 * @author Jesús Urrutia <jesus.urrutia@gmail.com>
 */
interface SearchInterface
{
    /**
     * Return array of search item elements
     *
     * @return SearchItemInterface[]
     */
    public function getResults();

    /**
     * Return number of total search item
     *
     * @return integer
     */
    public function getTotal();

    /**
     * Return page of results
     *
     * @return integer
     */
    public function getPage();

    /**
     * Return results per page
     *
     * @return integer
     */
    public function getLimit();

    /**
     * Return title search
     *
     * @return string
     */
    public function getTitle();
}
