<?php

namespace Linets\SearchBundle\Interfaces;


/**
 * Interface SearchItemInterface
 * @package Linets\SearchBundle\Interfaces
 *
 * @author Jesús Urrutia <jesus.urrutia@gmail.com>
 */
interface SearchItemInterface
{
    /**
     * @return string
     */
    public function getTitle();

    /**
     * @return string
     */
    public function getMore();

    /**
     * @return string
     */
    public function getUri();

    /**
     * @return string
     */
    public function getImage();

    /**
     * @return \DateTime
     */
    public function getPublished();
}
