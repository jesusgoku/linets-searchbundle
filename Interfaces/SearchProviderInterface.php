<?php

namespace Linets\SearchBundle\Interfaces;


/**
 * Interface SearchProviderInterface
 * @package Linets\SearchBundle\Interfaces
 *
 * @author Jesús Urrutia <jesus.urrutia@gmail.com>
 */
interface SearchProviderInterface
{
    const ENHANCED_SEARCH_SEARCH = 'search';
    const ENHANCED_SEARCH_EXACT = 'exact';
    const ENHANCED_SEARCH_EXCLUDE = 'exclude';
    const ENHANCED_SEARCH_TAG = 'tag';
    const ENHANCED_SEARCH_SINCE = 'since';
    const ENHANCED_SEARCH_UNTIL = 'until';

    /**
     * @param string $q
     * @param integer $offset
     * @param integer $limit
     * @param array $options
     * @return SearchInterface
     */
    public function search($q, $offset = 0, $limit = 10, array $options = array());

    /**
     * Return true when provider support enhanced search.
     *
     * When provider support enhanced search an associative array is pass in first parameter
     * to search method. With array keys of ENHANCED_SEARCH_*
     *
     * Example:
     * array(
     *  'search' => array('some', 'words', 'for', 'search'),
     *  'exact' => array('some phrase for search', 'other phrase'),
     *  'tag' => array('food', 'sports'),
     *  ...
     * )
     *
     * Note: Not all ENHANCED_SEARCH_* if presents
     *
     * @return boolean
     */
    public function supportEnhancedSearch();
}
