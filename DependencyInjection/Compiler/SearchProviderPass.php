<?php

namespace Linets\SearchBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;


/**
 * Class SearchProviderPass
 * @package Linets\SearchBundle\DependencyInjection\Compiler
 *
 * @author Jesús Urrutia <jesus.urrutia@gmail.com>
 */
class SearchProviderPass implements CompilerPassInterface
{
    /**
     * You can modify the container here before it is dumped to PHP code.
     *
     * @param ContainerBuilder $container
     *
     * @api
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition('linets.search.search')) {
            return;
        }

        $taggedServicesId = $container->findTaggedServiceIds('linets.search_provider');

        $appSearchDefinition = $container->getDefinition('linets.search.search');

        foreach ($taggedServicesId as $serviceId => $tags) {
            foreach ($tags as $tagAttributes) {
                $appSearchDefinition
                    ->addMethodCall('addSearchProvider', array(
                        $tagAttributes['alias'],
                        new Reference($serviceId),
                    ));
            }
        }
    }

}
