<?php

namespace Linets\SearchBundle;

use Linets\SearchBundle\DependencyInjection\Compiler\SearchProviderPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class LinetsSearchBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new SearchProviderPass());
    }
}
