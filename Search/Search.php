<?php

namespace Linets\SearchBundle\Search;

use Linets\SearchBundle\Interfaces\SearchInterface;
use Linets\SearchBundle\Interfaces\SearchProviderInterface;
use Symfony\Component\Routing\Exception\InvalidParameterException;


/**
 * Class Search
 * @package Linets\SearchBundle\Search
 *
 * @author Jesús Urrutia <jesus.urrutia@gmail.com>
 */
class Search
{
    /** @var SearchProviderInterface[] */
    protected $searchProviders;

    /**
     * @param SearchProviderInterface[] $searchProviders
     */
    public function __construct($searchProviders = array())
    {
        $this->searchProviders = array();

        foreach ($searchProviders as $name => $item) {
            if ($item instanceof SearchProviderInterface) {
                $this->searchProviders[$name] = $item;
            }
        }
    }

    /**
     * @param string $name
     * @param SearchProviderInterface $searchProvider
     */
    public function addSearchProvider($name, SearchProviderInterface $searchProvider)
    {
        $this->searchProviders[$name] = $searchProvider;
    }

    /**
     * @param string $q
     * @param int $offset
     * @param int $limit
     *
     * @return SearchInterface[]
     */
    public function search($q, $offset = 0, $limit = 10)
    {
        $search = $this->processSearchString($q);

        $found = array();
        foreach ($this->searchProviders as $sp) {
            $foundItem = $sp->supportEnhancedSearch()
                ? $sp->search($search, $offset, $limit)
                : $sp->search($q, $offset, $limit);

            if ($foundItem->getTotal() > 0) {
                $found[] = $foundItem;
            }
        }

        return $found;
    }

    /**
     * @param string $name
     * @param $q
     * @param int $offset
     * @param int $limit
     *
     * @return SearchInterface
     */
    public function searchOnProvider($name, $q, $offset = 0, $limit = 10)
    {
        if (!array_key_exists($name, $this->searchProviders)) {
            throw new InvalidParameterException('Not found provider');
        }

        return $this->searchProviders[$name]->search($q, $offset, $limit);
    }

    /**
     * @param string $q
     *
     * @return array
     */
    private function processSearchString($q)
    {
        $patterns = array(
            SearchProviderInterface::ENHANCED_SEARCH_SINCE => '/desde:([0-9]{1,2}-[0-9]{1,2}-[0-9]{4})\b/',
            SearchProviderInterface::ENHANCED_SEARCH_UNTIL => '/hasta:([0-9]{1,2}-[0-9]{1,2}-[0-9]{4})\b/',
            SearchProviderInterface::ENHANCED_SEARCH_TAG => '/tag:\(([^)]+)\)|tag:([\p{L}\d]+\b)/u',
            SearchProviderInterface::ENHANCED_SEARCH_EXCLUDE => '/-([\p{L}\d]+)\b/u',
            SearchProviderInterface::ENHANCED_SEARCH_EXACT => '/"([^"]+)"/',
            SearchProviderInterface::ENHANCED_SEARCH_SEARCH => '/([\p{L}\d]+)\b/u',
        );

        $result = array();
        foreach ($patterns as $key => $pattern) {
            $matches = array();
            preg_match_all($pattern, $q, $matches);
            if (!empty($matches[1])) {
                $result[$key] = $matches[1];
                // Remove found patterns
                foreach ($matches[0] as $item) {
                    $q = str_replace($item, '', $q);
                }
            }
        }

        return $result;
    }
}
